# 互联网医疗项目（华天）

# 项目技术环境概述 

* 使用 vscode 开发环境
* 本项目集成了 eslint 代码规范，vscode 中使用 setting 中 autoFixOnSave": true，确保保存文件时自动按指定 eslint rule 进行自动修复
* 本项目基础前端框架采用 vue/nuxt
* UI组件库使用 vuestrap，内置了 boostrap4 样式表；本项目无需 jquery 和 bootstrap.js
* 服务端调用引用的 plugins/spost，和沃行销，沃津眼调用方法完全一致

# 新老关系策略

* 保持原有网站的样式，风格
* 将原来网站分解，vue 组件化
* 各个 vue 组件对 js 控制部分更改为 vue 方式
* 各个 vue 组件调用后台接口供应动态数据渲染
* 对于原先使用 jquery 方式设置 ID 然后 $(id) 设置事件处理器的，改成 Vue 方式，原先设置的 id/class 保持不变(防止还有引用)
* 只保留原先的第三方js库，应用本身的js代码全部纳入vue
* 原有 id,name 等值vue化后不用了，对值后面添加_ 使得原有老 jq 代码失效
* 老项目的 STATIC 目录不进入到本项目版本管理，保持不变，开发部署时放到 static 目录下，也就是 static/STATIC
* ajax.load 的代码需要改造，接口只提供数据，不带页面

# 指导安排
* 导航栏中：首页，文化资讯，文化活动做范例，能静态跑通；其他项目参照同样方法由其他人纳入 Vue 项目，组件拆解
* 首先保证能静态跑通，所有 css, js 库能加载齐全

# 迁移到 vue 架构过程
* 下载本项目，yarn 安装第三方依赖
* 拷贝原系统 STATIC 目录到本项目 static/STATIC 位置，本目录内容不纳入版本控制，也严禁修改
* 原有页面路径对应 vue/nuxt 页面路径，完全不变
* 将原有页面内容拆分成不同的 vue 组件，在 components 中为每页单独设立子目录，归属组件放入期内
* 从原网站 debug 模式 elements 中选中区域拷贝，考到 vsc 新建 vue template 中，vsc 设置 html 格式命令排版，再改成 vue 格式命令排版
* 将原有页面特有css,js(非公共，非第三方)通过 nuxt head 纳入
* 研究其中的 js，将其中功能移植到 vue 中，完全移植完该 js 的引用注释掉

# V11 UPDATE 20181102-1
* 添加就诊人，手机号/身份证号验证
* 模板消息加连接，	
* 公众号菜单先添加一些静态内容，医生头像去掉（？）, 显示微信用户头像信息， 病人管理小图标更换
* 就诊缴费页，加入患者信息
* 切换订单状态列表
* 预约的页面顺序
* 付款成功后的操作（修改订单状态，订单支付状态），	

# V11 UPDATE 20181103-2
* 完成接口的封闭原理，并通过测试，后面可以以doctorPage页的方法，封闭所有页面的请求

# V11 UPDATE 2018-11-13-1
* 1	预约时，选择另外一个人后，预约时间由2018.11.8改成了2019.1.10
* 2	预约时，选择时间，列表显示后，没有滚动条，没有确定按钮。		OK
* 3	预约时，选择时间，12：00 - 13：00 不能预约，可以不显示。		OK
* 4	预约时，今天不能被预约，不需要显示。							OK
* 5	预约时，不能预约的时间，建议“预约挂号”按钮显示为灰色。			OK

# V11 UPDATE 2018-11-13-2
* 1	预约时，选择另外一个人后，预约时间由2018.11.8改成了2019.1.10

# V11 UPDATE 2018-11-13-3

# V11 UPDATE 2018-11-13-4
* 差两个页的分页，修改病人与收货地址

# V12
* 增加医生列表功能

# V13
* 年前大改动前备份，去掉蓝色标题行，优化页面样式前的备份

# V14
* 1.医生列表更新
* 2.与患者留言两页页面增加到四个界面的逻辑，更新完两个，还有两个未更新

# V15
* 1.完成患者主业务流程
* 2.地址管理与订单支付管理未完成

# V16
* 1.更新5个功能
* 2.明天领导确认完页面，完成剩下两个后台的功能

# V17
* 1.V16未改完

# V18
* 1.医生留言页未完成
* 2.其它均完成

# V19
* 1.完成本地与远程切换开关
* 2.app.html中MODE，pro为生产模式，dev为开发模式


# V20
* 1.页面进一步优化 

# V21
* 1.页面进一步优化到调起支付页

# V22
* 1.调通支付功能，暂不能发送支付后的模板消息
* 2.订单流程中的几个状态页组件化

# V23
* 1.继续完善页面

# V23-1

# V23-2
1.  调通全部功能，部分细节还需要完善

# V24
1.  调通患者端全部功能
2.  优化患者选药店，医师选药店的后台代码
3.  更新支付条件

# V25
1.  更新几个小细节和支付流程，下一步完善发货与发货后的显示功能。

# V26
2.  发布版V1


##2019-04-04  v27
1.  更新登录BUG

## Build Setup

``` bash
# install dependencies
$ npm -g install yarn
$ yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
