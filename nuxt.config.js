/*
 * 用于组织Nuxt.js应用的个性化配置，已覆盖默认配置
 */
module.exports = {
  /*   transition: {
    name: 'fade',
    mode: 'out-in',
  }, */
  resourceHints: 'false',
  // 页面切换的进度条
  loading: {
    color: 'green',
    height: '2px',
  },
  loadingIndicator: {
    name: 'circle',
    color: '#3B8070',
    background: 'white',
  },
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: '欢迎光临',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'viewport-fit-cover,width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
    script: [
      { src: '' },
    ],
  },
  css: [
    'animate.css/animate.min.css',
    'assets/main.css',
    'assets/reset.css',
  ],
  // loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  router: {
    base: '/nethospital',
  },
  modules: [],
  plugins: [
    '~/plugins/spost',
    '~/plugins/mint.js',
    '~/plugins/iutils',
    '~/env',
    '~/plugins/scroll.js',
    '~/plugins/scroller.js',
  ],
};
