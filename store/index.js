// 本文件应该放到 nuxt 项目的 store 子目录中，作为登录会话模块
// spost 自动将 vuex 的登录成功信息加入每个请求中，做有会话服务端调用

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// window.fetch() 的 Polyfill
// require('whatwg-fetch');

const store = () => new Vuex.Store({
  strict: true,
  state: {
    cookies: {
      hospitalName: '',
      hospitalDomainname: '',
      hospitalDlid: '',
      SESSION_ID: '',
      STAFF_ID: 'LTHXY130',
    },
    counter: 0,
  },
  mutations: {
    login(state, token) {
      state.cookies.SESSION_ID = token;
    },
    increment(state) {
      state.counter += 1;
    },
    setToken(state, token) {
      state.cookies.SESSION_ID = token;
    },
    setHospitalName(state, token) {
      state.cookies.hospitalName = token;
    },
    setHospitalDomainname(state, token) {
      state.cookies.hospitalDomainname = token;
    },
    setHospitalDlid(state, token) {
      state.cookies.hospitalDlid = token;
    },
  },

  actions: {
    // ...
  },

});

export default store;
