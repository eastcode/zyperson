// import PromiseState from './PromiseState';

/**
 * BOOSHULI
 * 2018-09-30
 * RequestData 自己是一个类，封装所有页面的请求接口内容
 * 返回json格式的字符串，发送到后台
 */

const { config } = window;

class RequestData {
  constructor() {
    this.VERSION = '1.0';
    this.TN = 'F27.APP.05.04';
    this.CLIENTID = 'YLAPP';
    this.RYBID = config.APIAPPID || 'PAT'; // 判断是否为调试，调试模式为''  | config.APIAPPID || 'PAT';
    this.TOKEN = '';
    this.DATA = {};

    // this.RESULT = {};
    // this.ts = new PromiseState();
  }

  /**
   * 拼接请求参数
   * @param {TN} tn
   * @param {RYBID} rid
   * @param {请求参数} dt
   */
  InitParam( tn, dt) {
    const ts = this;
    ts.TN = tn;
    ts.DATA = JSON.stringify(dt);
    ts.TOKEN = `${localStorage.getItem('RYBTOKEN.PAT') == null ? '' : localStorage.getItem('RYBTOKEN.PAT')}`; // 判断是否为调试，调试模式为''，若不为空，从store中取当前登录者TOKEN

    /**
     * 对请求的数据进行筛选判断处理
     */


    const reqStr = `{"OTRequest":{"VERSION":"${ts.VERSION}","TN":"${ts.TN}","CLIENTID":"${ts.CLIENTID}","RYBID":"${ts.RYBID}","TOKEN":"${ts.TOKEN}","DATA":${ts.DATA}}}`;
    console.log(`发出请求：${reqStr}`);

    /**
     * 此处可对请求进行加密处理
     */


    return `${reqStr}`;
  }
}

export default RequestData;
