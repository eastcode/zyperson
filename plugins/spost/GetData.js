import RequestData from './RequestData';

const { config } = window;

/**
 * BAOSHULI
 * 获取后台数据接口
 * 2018-10-23
 */
class GetData {
  constructor() {
    this.d = {};
    this.rd = new RequestData();
  }


  /**
   * 获取医生详细信息
   * @param {
   *
   * } data 请求参数
   * @returns {*
   *  ""
   * }
   */
  getDoctorDetail(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.05.09', data) });
  }
  /**
   * 获取医生排班时间详细信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getDoctorPaibanInfo(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.11', data) });
  }
  /**
   * 获取当前登录用户的默认病人 ?-1
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getPatientDefaulty(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.04', data) });
  }

  /**
   *  保存预约信息 ?-2
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  saveYuYue(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.05.05', data) });
  }

  /**
   * 返回该患者收药地址列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getAddr(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.27', data) });
  }

  /**
   * 付款
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  perPay(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.30', data) });
  }


    /**
   * 付款
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  doPay(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.37', data) });
  }

  /**
   * 获取处方订单的支付信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getPayOrderInfo(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.38', data) });
  }



  /**
   * 获取处方订单的详情信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getOrderDetail(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.43', data) });
  }



  /**
   * 微信相关
   * @param {
   * } data 请求参数
   * @returns {*}
   */
  getWxConfigParam(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.00.09', data) });
  }
  /**
   * 添加地址
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  addAddress(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.26', data) });
  }
  /**
   * 保存修改的地址信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  saveEditAddress(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.26', data) });
  }
  /**
   * 保存新的病人信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  saveNewPatient(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.01', data) });
  }
  /**
   * 删除地址
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  removeadr(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.28', data) });
  }
  /**
   * 获取地址
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getaddress(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.27', data) });
  }
  /**
   * 切换地址
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  changeAdr(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.33', data) });
  }

  /**
   * 删除病人
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  removePat(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.02', data) });
  }
  /**
   * 获得病人列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getPatientList(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.05', data) });
  }
  /**
   * 修改默认病人
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  changePat(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.06', data) });
  }
  /**
   * 获取医生排班列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getDoctorPaibanlist(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.05.04', data) });
  }
  /**
   * 获取就诊列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getJzlist(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.05.08', data) });
  }
  /**
   * 获取预约列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getYuyuelist(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.05.08', data) });
  }
  /**
   * 获取某个患者的信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getPatInfo(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.12', data) });
  }
  /**
   * 保存修改的患者信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  saveEditPatient(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.06.03', data) });
  }
  /**
   * 获取前置机订单支付信息
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getPayInfo(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.12.02', data) });
  }
  /**
   * 获取医院医生列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getDoctoList(data, ctx) {
    const gd = this;

    const rvObject = ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.18', data) });

    return rvObject;

  }


    /**
   * 获取患者所有的处方订单列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getallprescribeOrderList(data, ctx) {
    const gd = this;

    const rvObject = ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.36', data) });

    return rvObject;

  }


  
    /**
   * 患者删除订单(逻辑删除)
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  deletePay(data, ctx) {
    const gd = this;

    const rvObject = ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.24', data) });

    return rvObject;

  }



    
    /**
   * 患者确认订单收货
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  queren(data, ctx) {
    const gd = this;

    const rvObject = ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.29', data) });

    return rvObject;

  }




  
  /**
   * 添加医保卡
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  addYbk(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.39', data) });
  }

    
  /**
   * 修改医保卡
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  modifyYbk(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.40', data) });
  }


  
  /**
   * 获取医保卡列表
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getYbkList(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.42', data) });
  }


    
  /**
   * 获取医保卡详情
   * @param {
   *
   * } data 请求参数
   * @returns {*}
   */
  getYbkDetail(data, ctx) {
    const gd = this;
    // let abcd;
    return ctx.spost(ctx.ts, config.APIINTERFACE, { '': gd.rd.InitParam('F27.APP.02.41', data) });
  }
}

export default GetData;
