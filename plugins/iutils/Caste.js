/**
 * 2018-10-22
 * BOOSHULI
 * 医生职业等级
 * zr-主任医师/fzr-副主任医师/zz-主治医师/ys-医师/zl-助理医师/kf-康复理疗师
 *
 */


class Caste {
  constructor() {
    this.sectionName = '普通门诊';
    this.sectionMoney = '10';
  }
  getCasteName(cn) {
    const d = this;
    switch (cn) {
      case 'zr':
        d.sectionName = '专家门诊';
        d.sectionMoney = '20';
        break;
      case 'fzr':
        d.sectionName = '专家门诊';
        d.sectionMoney = '20';
        break;
      default:
        d.sectionName = '普通门诊';
        d.sectionMoney = '10';
    }
    console.log(`--------------------------：：${d.sectionName}&nbsp;&nbsp;${d.sectionMoney}￥`);
    // return `${d.sectionName}  ${d.sectionMoney}￥`;
    return `${d.sectionName}`;
  }
}

export default Caste;
