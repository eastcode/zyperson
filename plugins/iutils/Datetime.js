/**
 * BSL
 * 2018-10-10
 * VUE工具类
 */

class Datetime {
  constructor(dayCount) {
    this.dayCount = dayCount;
    this.dt = new Date();
    this.dt.setDate(this.dt.getDate() + dayCount);
    this.seperator1 = '-';
    this.year = this.dt.getFullYear();
    this.month = this.dt.getMonth() + 1;
    this.day = this.dt.getDate();
    if (this.month >= 1 && this.month <= 9) {
      this.month = `0${this.month}`;
    }
    if (this.strDate >= 0 && this.strDate <= 9) {
      this.strDate = `0${this.strDate}`;
    }
  }
  // 获取当前周几，格式周一，周二 头部导航用
  getDateM(dayCount) {
    const d = this;
    let str = '';
    if (dayCount == 0) {
      str = '今&nbsp;天<br>';
    } else {
      str = `周&nbsp;${'日一二三四五六日一二三四五六'.charAt(new Date().getDay() + dayCount)}<br>`;
    }
    return str + d.month + d.seperator1 + d.day;
  }
}

export default Datetime;
