/**
 * BSL
 * 2018-11-12
 * 正则表达式，验证手机号，身份证号等等
 */

class Regular {
  constructor(str) {
    this.str = str;
  }

  /**
   * @name 判断输入内容是否为空
   * @param str 输入字符串
   * @returns
   */
  IsNull(str) { const s = this; let resu = true; if (str.length != 0) { resu = false; } return resu; }

  /**
   * @name 验证手机号码部分
   */
  IsMobile(str) {
    const s = this;
    let resu = true;
    const reg = 11 && /^((13|14|15|16|17|18)[0-9]{1}\d{8})$/;
    if (!reg.test(str)) { resu = false; } return resu;
  }

  /**
   * @name 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
   */
  IsPersonId(str) {
    const s = this;
    let resu = true;
    const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    if (!reg.test(str)) { resu = false; } return resu;
  }
}
export default Regular;
