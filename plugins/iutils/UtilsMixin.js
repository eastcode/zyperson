/**
 * BSL
 * 2018-10-10
 * 可以引入更多的工具类
 */
import Dtime from './Datetime'; // 时间类
import Caste from './Caste'; // 医生职业等级
import Regular from './Regular'; // 正则验证


export default {
  methods: {
    dtime(d) {
      return new Dtime(d);
    },
    caste() {
      return new Caste();
    },
    regular(str) {
      return new Regular(str);
    },
  },
};
