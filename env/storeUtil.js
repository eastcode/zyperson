

class Store {
  constructor() {
    this.name = '';
    this.content = '';
  }
  /**
   * 存储localStorage
   */
  setLocalStore(name, content) {
    const d = this;
    if (!name) return;
    if (typeof content !== 'string') {
      content = JSON.stringify(content);
    }
    window.localStorage.setItem(name, content);
  }

  /**
 * 获取localStorage
 */
  getLocalStore(name) {
    const d = this;
    if (!name) return;
    return window.localStorage.getItem(name);
  }

  /**
 * 删除localStorage
 */
  removeLocalStore(name) {
    const d = this;
    if (!name) return;
    window.localStorage.removeItem(name);
  }

  /**
 * 存储sessionStorage
 */
  setSessionStore(name, content) {
    const d = this;
    if (!name) return;
    if (typeof content !== 'string') {
      content = JSON.stringify(content);
    }
    window.sessionStorage.setItem(name, content);
  }

  /**
 * 获取sessionStorage
 */
  getSessionStore(name) {
    const d = this;
    if (!name) return;
    return window.sessionStorage.getItem(name);
  }

  /**
 * 删除sessionStorage
 */
  removeSessionStore(name) {
    const d = this;
    if (!name) return;
    window.sessionStorage.removeItem(name);
  }
}

export default Store;
